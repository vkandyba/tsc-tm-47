package ru.vkandyba.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.service.IConnectionService;

public abstract class AbstractService {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
