package ru.vkandyba.tm.service.dto;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.dto.TaskDTO;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskDtoService extends AbstractDtoService {

    public TaskDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    public List<TaskDTO> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findAll(userId);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public TaskDTO findByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public TaskDTO findById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findByName(userId, id);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void add(String userId, TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            task.setUserId(userId);
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void removeById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.removeByName(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        if(description == null || description.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.updateById(userId, id, name, description);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void startById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.startById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void startByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.startByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void finishById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.finishById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void finishByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.finishByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public List<TaskDTO> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void bindTaskToProjectById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.bindTaskToProjectById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void unbindTaskToProjectById(@Nullable String userId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.unbindTaskToProjectById(userId, taskId);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void removeAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
