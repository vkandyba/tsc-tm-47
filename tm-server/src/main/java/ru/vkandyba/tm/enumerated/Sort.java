package ru.vkandyba.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.comparator.ComparatorByCreated;
import ru.vkandyba.tm.comparator.ComparatorByName;
import ru.vkandyba.tm.comparator.ComparatorByStartDate;
import ru.vkandyba.tm.comparator.ComparatorByStatus;

import java.util.Comparator;


@Getter
public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created date", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());
    @NotNull
    private final String displayName;
    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
