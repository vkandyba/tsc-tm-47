package ru.vkandyba.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.SessionDTO;
import ru.vkandyba.tm.model.Session;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractRepository<Session> {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }


}
