package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.service.dto.ProjectDtoService;
import ru.vkandyba.tm.service.dto.SessionDtoService;
import ru.vkandyba.tm.service.dto.TaskDtoService;
import ru.vkandyba.tm.service.dto.UserDtoService;
import ru.vkandyba.tm.service.model.ProjectService;
import ru.vkandyba.tm.service.model.SessionService;
import ru.vkandyba.tm.service.model.TaskService;
import ru.vkandyba.tm.service.model.UserService;

public interface ServiceLocator {

    TaskService getTaskService();

    ProjectService getProjectService();

    UserService getUserService();

    IPropertyService getPropertyService();

    SessionService getSessionService();

    ProjectDtoService getProjectDtoService();

    TaskDtoService getTaskDtoService();

    SessionDtoService getSessionDtoService();

    UserDtoService getUserDtoService();

}
