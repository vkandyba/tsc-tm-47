package ru.vkandyba.tm.api.repository;

import org.apache.ibatis.annotations.Insert;
import ru.vkandyba.tm.dto.SessionDTO;

public interface ISessionRepository extends IRepository<SessionDTO> {

    @Insert("INSERT INTO app_sessions(id, user_id, signature, timestamp) " +
            "VALUES(#{id}, #{userId}, #{signature}, #{timestamp})")
    void add(SessionDTO sessionDTO);

}
