package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.endpoint.*;
import ru.vkandyba.tm.service.PropertyService;

public interface ServiceLocator {

    SessionDTO getSession();

    void setSession(SessionDTO session);

    SessionEndpoint getSessionEndpoint();

    ProjectEndpoint getProjectEndpoint();

    TaskEndpoint getTaskEndpoint();

    UserEndpoint getUserEndpoint();

    AdminEndpoint getAdminEndpoint();

    ICommandService getCommandService();

    IPropertyService getPropertyService();

}
